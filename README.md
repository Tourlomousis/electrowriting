This page is served under: https://tourlomousis.pages.cba.mit.edu/electrowriting/

<h2>Notes</h2>
- Material:
    - CORBION:
        - USA Contact: https://www.corbion.com/about-corbion/contact/north-america
        - Options: https://www.corbion.com/biomedical/products/polymers-for-medical-devices
        - PCL: https://www.corbion.com/static/downloads/datasheets/23d/PURASORB%20PC%2012.pdf
    - Ferroelectric:
        - Poly K
            - http://www.polyk-lab.com/PVDF-TRFE
        - SIGMA
            - https://www.sigmaaldrich.com/catalog/search?term=28960-88-5&interface=CAS%20No.&N=0&mode=partialmax&lang=en&region=US&focus=product
        - PIEZOTECH:
            - https://www.piezotech.eu/en/Technical-center/Documentation/
            - https://www.piezotech.eu/en/Products/Powders/
        
- Enclosure  
    - Hinges: https://www.mcmaster.com/#aluminum-extrusion-hinges/=1dbzz8q
    
- Rotary Collector for 4-axes fiber printing
     - Linear Motion Shafts - Both ends Threaded: https://goo.gl/2khPYL

- Fans for Cooling: https://goo.gl/B9u91r
 
- Cheaper Voltage Supplies:
    - https://www.xppower.com/Product/DX-Series

<h2>Tasks</h2>
<h3>TO DO</h3>
- water jet one more aluminum part with cut through holes
- milling 
    - reduce thickness of each aluminum part
    - pockets in every part for hetaers, electrodes etc.
    - threading of holes (manual?)
    - PEEK skins

- CAD for stepless pneumatic pump
- CAD for rotary collector - 4-axes fiber printing
- CAD for enclosure 

<h3>DONE</h3>
- CAD for lead screw driven pump

- Aluminum Part Outline Cut
    - SOS you forgot to do also cut through bigger holes for mounting them together.  
      So you will have to cut one more aluminum part in the water jet.

- ORDERED:
    - voltage supply (RECEIVED)
        - LINK: 
        
    - SMC electro-pneumatic pressure regulator (RECEIVED)
        - LINK: http://smcpartbuilder.com/products/Airline-Equipment/Pressure-Regulators/Electronic-Regulators/ITV/ITV100020003000-Electro-Pneumatic-Regulator~127735
        - CONFIGURATION Number: ITV1050-31N2CS4
            - 200 L/min
            - 1(24VDC)
            - Input Signal: Voltage Style 0~10VDC
            - Analog Output 1~5VDC
        - Useful Videos
            - https://www.youtube.com/watch?v=CkS3ZBF22qw
            - https://www.youtube.com/watch?v=ZdLlOVKGWkE
            
    - grounded collector & accessories (RECEIVED) 
        - SHOPPING CART: https://www.mcmaster.com/#orders/=1dge9o9
        
    - pneumatics accessories
        - SHOPPING CART: http://www.smcpneumatics.com/view_cart.asp
        - Electro-pneumatic Regulator Accessories: 
        - Male Connectors (1/4"NPT, Port Size: O.D.=13.2 mm, Tubing Size: O.D.=3/8") (RECEIVED)
            - Configuration: KQ2H11-35NS
        - Male Connector (1/8" NPT, Port Size: O.D.=9.9 mm, Tubing Size: O.D.= 3/8") (RECEIVED)
            - Link: https://goo.gl/hbiy35
            - Configuration: KQ2L11-34NP (elbow) or KQ2H11-34NP 
        - Female Connector (1/8"NPT, Port Size: O.D.=9.9mm, Tubing Size: O.D.=1/4") (RECEIVED)
            - Link: https://goo.gl/nBv9kg OR https://goo.gl/m6qapb (elbow)
            - Configuration: KQ2F07-34N or KQ2F07-34N(elbow)
        - Female Connector (1/4"NPT, Port Size: O.D.= 13.2mm, Tubing Size: O.D.=1/4") (RECEIVED)
            - Link: https://goo.gl/Vc9zme
            - Configuration: KQ2F07-35N
        - Tubing (O.D.=3/8")                (RECEIVED)
            - Link: https://goo.gl/U9JzZh
            - Configuration: TIUB11YR-20
        - Tubing (O.D.=1/4")                (RECEIVED) 
            - Link: https://goo.gl/iH5Y2z
            - Configuration: TIUB07G-20
        